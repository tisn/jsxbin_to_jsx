﻿using Jsbeautifier;
using jsxbin_to_jsx.JsxbinDecoding;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace jsxbin_to_jsx_GUI
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string jsxbin = richTextBox1.Text;
            string jsx = AbstractNode.Decode(jsxbin, true);
            //sjsx = new Beautifier().Beautify(jsx);
            richTextBox2.Text = jsx;
        }
    }
}
