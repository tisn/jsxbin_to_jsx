﻿using Jsbeautifier;
using jsxbin_to_jsx.JsxbinDecoding;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace jsxbin_to_jsx_GUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetData(DataFormats.FileDrop) != null)
            {
                e.Effect = DragDropEffects.Copy;
            }
        }

        private void Form1_DragDrop(object sender, DragEventArgs e)
        {
            String[] fileNames = (String[])e.Data.GetData(DataFormats.FileDrop);
            for (int i = 0; i < fileNames.Length; i++)
            {
                var f = fileNames[i];
                AddFile(f);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem l in listView1.Items)
            {
                string f = l.SubItems[1].Text;
                string jsxbin = File.ReadAllText(f, Encoding.ASCII);
                string jsx = AbstractNode.Decode(jsxbin, true);
                jsx = new Beautifier().Beautify(jsx);
                string of = Path.GetDirectoryName(f) + "\\" + Path.GetFileNameWithoutExtension(f) + ".js";
                File.WriteAllText(of, jsx, Encoding.UTF8);
                l.SubItems[2].Text = "已解密";
            }
        }

        private void AddFile(string f)
        {
            //var ext = Path.GetExtension(f).ToLower();
            //if (Array.IndexOf(new String[] {".jsx",".jsx.bin",".jsxbin"}, ext) == -1)
                //return;
            ListViewItem lvi = new ListViewItem(Path.GetFileNameWithoutExtension(f));
            lvi.SubItems.Add(f);
            lvi.SubItems.Add("未解密");
            listView1.Items.Add(lvi);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                foreach(var f in openFileDialog1.FileNames)
                {
                    AddFile(f);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            new Form2().Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var fn = @"Z:\Crack\AI脚本插件合集v8.1\AI脚本插件合集v8.1_2021_64.jsx";
            var txt = File.ReadAllText(fn);

            var mev = new Regex(@"eval\('([\s\S\r\n]*?)'\)");
            var varNames = "(jxMessage|js2Message|swap1Message|epMessage|js48Message|js35Message)";
            var ms = Regex.Matches(txt, @"var (\w*?) *= *""([^""]*?)"" *; *[\r\n]*? *(EPMsg|epinv) *\( *(\w*?) *\) *;", RegexOptions.Multiline);

            var newTxt = "";
            for(var i = ms.Count - 1; i >= 0; i--)
            {
                var m = ms[i];
                string jsxbin = m.Groups[2].Value;
                var mc = mev.Match(jsxbin);
                if (mc.Success)
                    jsxbin = mc.Groups[1].Value;
                string jsx = AbstractNode.Decode(jsxbin, true);
                var g = m.Groups[0];
                txt = txt.Substring(0, g.Index) + jsx + txt.Substring(g.Index + g.Length);
            }

            ms = Regex.Matches(txt, @"eval\('([\s\S\r\n]*?)'\);");
            for (var i = ms.Count - 1; i >= 0; i--)
            {
                var m = ms[i];
                string jsxbin = m.Groups[1].Value;
                string jsx = AbstractNode.Decode(jsxbin, true);
                var g = m.Groups[0];
                txt = txt.Remove(g.Index, g.Length);
                txt = txt.Insert(g.Index, jsx);
            }
            File.WriteAllText(fn + ".js", txt);
        }
    }
}
