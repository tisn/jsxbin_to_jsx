﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jsxbin_to_jsx.JsxbinDecoding
{
    public sealed class ReferenceDecoderVersion21 : IReferenceDecoder
    {
        public double JsxbinVersion
        {
            get
            {
                return 2.1;
            }
        }

        public Tuple<string, bool> Decode(INode node)
        {
            var id = node.DecodeId();
            var flag = node.DecodeBool();
            return Tuple.Create(id, flag);
        }
    }
}
